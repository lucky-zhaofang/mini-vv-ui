export default [
  {
    text: '导航',
    link: '/guide/',
    activeMatch: '/guide/'
  },
  {
    text: '组件',
    link: '/components/',
    activeMatch: '/components/'
  },
  {
    text: '友情链接',
    items: [{ text: 'gitee', link: 'https://gitee.com/lucky-zhaofang' }]
  }
]
